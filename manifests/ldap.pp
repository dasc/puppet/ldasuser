define ldasuser::ldap ($username = $title, $uid, $homefilesystem = 'home4', $groups = [], $altnames = [], $ensure = 'present' ) {
    include ::ldasuser

#    if $::ldasuser::localscratchbase and $::ldasuser::localscratchbase != '' {
#	file { "${::ldasuser::localscratchbase}/${username}":
#	    ensure => 'directory',
#	    owner => $uid,
#	    group => $uid,
#	    mode => '0755',
#	}
#    }

    if length($groups) > 0 {
	user { "${title}":
	    ensure => present,
	    uid => $uid,
	    forcelocal => false,
	    managehome => false,
	    groups => $groups,
	}

	file_line { "passwd_${title}":
	    ensure => absent,
	    path => '/etc/passwd',
	    match => "${title}:",
	    match_for_absence => true,
	    multiple => true,
	} -> User["${title}"]
	file_line { "shadow_${title}":
	    ensure => absent,
	    path => '/etc/shadow',
	    match => "${title}:",
	    match_for_absence => true,
	    multiple => true,
	} -> User["${title}"]
	file_line { "group_${title}":
	    ensure => absent,
	    path => '/etc/group',
	    match => "${title}:",
	    match_for_absence => true,
	    multiple => true,
	} -> User["${title}"]
    }

    if $altnames =~ String {
	file_line { "autofs_${altnames}":
	    ensure => "${ensure}",
	    path => "/etc/auto.aliases",
	    line => sprintf("%-32s -fstype=bind  :/mnt/${homefilesystem}/${title}", "/home/${altnames}"),
	    require => File['/etc/auto.aliases'],
	}
    } else {
	# Either altnames is an array of strings, or it's the null set
	$altnames.each |$altname| {
	    file_line { "autofs_${altname}":
		ensure => "${ensure}",
		path => "/etc/auto.aliases",
		line => sprintf("%-32s -fstype=bind  :/mnt/${homefilesystem}/${title}", "/home/${altname}"),
		require => File['/etc/auto.aliases'],
	    }
	}
    }

#    if $::ldasuser::usehomedirlink {
#	file { "/home/${username}":
#	    ensure => link,
#	    target => "/mnt/${homefilesystem}/${username}",
#	}
#    }
#    file { "/archive/home/${username}":
#        ensure => link,
#        target => "/home/${username}",
#    }
}
