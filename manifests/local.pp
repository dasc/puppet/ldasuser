define ldasuser::local ($username = $title, $uid, $gid, $homefilesystem = 'home4', $shell, $groups = [], $altnames = [], $ensure = 'present') {
    include ::ldasuser

    $password = $::ldasuser::disable_local_login ? {
        true  => "!!",
        false => "x",
    }

#    if $::ldasuser::localscratchbase and $::ldasuser::localscratchbase != '' {
#        file { "${::ldasuser::localscratchbase}/${username}":
#            ensure => 'directory',
#            owner  => $uid,
#            group  => $gid,
#            mode   => '0755',
#        }
#    }

    if size($altnames) > 0 {
        $allowdupe = 'true'
    } else {
        $allowdupe = 'false'
    }

    if $ensure == 'absent' {
        user { "${title}":
            ensure => "${ensure}",
        }
    } else {
        user { "${title}":
            ensure     => "${ensure}",
            uid        => $uid,
            gid        => $gid,
            forcelocal => true,
            home       => "/home/${username}",
            managehome => false,
            shell      => $shell,
            groups     => $groups,
            password   => $password,
            allowdupe  => $allowdupe,
        }
    }

#    if $altnames =~ String {
#        file_line { "autofs_${altnames}":
#            ensure => "${ensure}",
#            path => "/etc/auto.aliases",
#            line => sprintf("%-32s -fstype=bind  :/mnt/${homefilesystem}/${title}", "/home/${altnames}"),
#            require => File['/etc/auto.aliases'],
#        }
#    } else {
#        $altnames.each |$altname| {
#            file_line { "autofs_${altname}":
#                ensure => "${ensure}",
#                path => "/etc/auto.aliases",
#                line => sprintf("%-32s -fstype=bind  :/mnt/${homefilesystem}/${title}", "/home/${altname}"),
#                require => File['/etc/auto.aliases'],
#            }
#        }
#    }

#    if $::ldasuser::usehomedirlink {
#        file { "/home/${username}":
#            ensure => link,
#            target => "/mnt/${homefilesystem}/${username}",
#        }
#    }
#    file { "/archive/home/${username}":
#        ensure => link,
#        target => "/home/${username}",
#    }
}
